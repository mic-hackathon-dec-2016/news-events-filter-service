import os

import MySQLdb as mdb
import boto3

queue_name_news = os.getenv('QUEUE_NAME')
print(queue_name_news)
aws_region = os.getenv('AWS_DEFAULT_REGION')
print(aws_region)
aws_access_id = os.getenv('AWS_ACCESS_KEY_ID')
print(aws_access_id)
aws_secret_key = os.getenv('AWS_SECRET_ACCESS_KEY')
print(aws_secret_key)
db_host = os.getenv('DB_HOST')
print(db_host)
db_user = os.getenv('DB_USER')
print(db_user)
db_password = os.getenv('DB_PASSWORD')
print(db_password)
db_name = os.getenv('DB_NAME')
print(db_name)


def create_table():
    con = mdb.connect(db_host, db_user, db_password, db_name, connect_timeout=5)
    with con:
        cur = con.cursor()
        cur.execute(
            "CREATE TABLE IF NOT EXISTS NEWS_FEED_PROCESSOR(Id INT NOT NULL AUTO_INCREMENT, Title VARCHAR(300), Description VARCHAR(300), Category VARCHAR(300), Link VARCHAR(300), PRIMARY KEY (Id))")


def insert_values_news(title, description, category, link):
    con = mdb.connect(db_host, db_user, db_password, db_name, connect_timeout=5)
    with con:
        cur = con.cursor()
        title = str(mdb.escape_string(title))
        description = str(mdb.escape_string(description))
        category = str(mdb.escape_string(category))
        link = str(mdb.escape_string(link))
        cur.execute("INSERT INTO NEWS_FEED_PROCESSOR (Title, Description, Category, Link) VALUES"
                    " ('" + title + "','" + description + "','" + category + "','" + link + "')")
        return cur.lastrowid


def process_news_feeder():
    create_table()
    sqs = boto3.resource('sqs')
    queue = sqs.get_queue_by_name(QueueName=queue_name_news)
    for message in queue.receive_messages(MessageAttributeNames=['title', 'description', 'Category', 'link']):
        print(message)
        if message.message_attributes is not None:
            title = message.message_attributes.get('title').get('StringValue')
            description = message.message_attributes.get('description').get('StringValue')
            link = message.message_attributes.get('link').get('StringValue')
            category = message.message_attributes.get('category').get('StringValue')
            insert_values_news(title, description, category, link)
            print(title)
            print(description)
            print(category)
            print(link)


process_news_feeder()

