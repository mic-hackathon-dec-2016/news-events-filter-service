import os

import MySQLdb as mdb
import boto3

queue_name_user = os.getenv('QUEUE_NAME')
print(queue_name_user)
aws_region = os.getenv('AWS_DEFAULT_REGION')
print(aws_region)
aws_access_id = os.getenv('AWS_ACCESS_KEY_ID')
print(aws_access_id)
aws_secret_key = os.getenv('AWS_SECRET_ACCESS_KEY')
print(aws_secret_key)
db_host = os.getenv('DB_HOST')
print(db_host)
db_user = os.getenv('DB_USER')
print(db_user)
db_password = os.getenv('DB_PASSWORD')
print(db_password)
db_name = os.getenv('DB_NAME')
print(db_name)


def create_table():
    con = mdb.connect(db_host, db_user, db_password, db_name, connect_timeout=5)
    with con:
        cur = con.cursor()
        cur.execute(
            "CREATE TABLE IF NOT EXISTS USER_FEED_PROCESSOR(Id INT NOT NULL, Name VARCHAR(100) NOT NULL, Subscription VARCHAR(300) NOT NULL, Frequency VARCHAR(3) NOT NULL, News VARCHAR(300), LastSent DATETIME)")


def insert_values_user(id, name, subscription, frequency):
    con = mdb.connect(db_host, db_user, db_password, db_name, connect_timeout=5)
    with con:
        cur = con.cursor()
        cur.execute("INSERT INTO USER_FEED_PROCESSOR (Id, Name, Subscription, Frequency) VALUES"
                    " (" + id + ",'" + name + "','" + subscription + "','" + frequency + "')")
        return cur.lastrowid


def process_user_subscription():
    create_table()
    sqs = boto3.resource('sqs')
    queue = sqs.get_queue_by_name(QueueName=queue_name_user)
    for message in queue.receive_messages(MessageAttributeNames=['Id', 'Name', 'Subscription', 'Frequency']):
        print(message)
        if message.message_attributes is not None:
            id = message.message_attributes.get('Id').get('StringValue')
            name = message.message_attributes.get('Name').get('StringValue')
            subscription = message.message_attributes.get('Subscription').get('StringValue')
            frequency = message.message_attributes.get('Frequency').get('StringValue')
            print("Inserting values for user from message queue.")
            insert_values_user(id, name, subscription, frequency)
            print(id)
            print(subscription)
            print(name)
            print(frequency)

process_user_subscription()
