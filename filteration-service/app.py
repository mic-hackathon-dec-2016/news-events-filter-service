import json
import os

import MySQLdb as mdb
from flask import Flask, jsonify, Response
from flask import abort
from flask import make_response

app = Flask(__name__)

db_host = os.getenv('DB_HOST')
print(db_host)
db_user = os.getenv('DB_USER')
print(db_user)
db_password = os.getenv('DB_PASSWORD')
print(db_password)
db_name = os.getenv('DB_NAME')
print(db_name)


def write_file(data, filename):
    with open(filename, 'w') as f:
        f.write(data)


def read_file(filename):
    with open(filename, 'r') as f:
        data = f.read()
    return data


def create_table():
    con = mdb.connect(db_host, db_user, db_password, db_name, connect_timeout=5)
    with con:
        cur = con.cursor()
        cur.execute(
            "CREATE TABLE IF NOT EXISTS USER_FEED_PROCESSOR(Id INT NOT NULL, Name VARCHAR(100) NOT NULL, Subscription VARCHAR(300) NOT NULL, Frequency VARCHAR(3) NOT NULL, News VARCHAR(300), LastSent DATETIME)")
        cur.execute(
            "CREATE TABLE IF NOT EXISTS NEWS_FEED_PROCESSOR(Id INT NOT NULL AUTO_INCREMENT, Title VARCHAR(300), Description VARCHAR(300), Category VARCHAR(300), Link VARCHAR(300), PRIMARY KEY (Id))")


def update_news_for_user(id, news):
    con = mdb.connect(db_host, db_user, db_password, db_name, connect_timeout=5)
    with con:
        cur = con.cursor()
        cur.execute("UPDATE USER_FEED_PROCESSOR SET News = '" + news + "' WHERE id = " + str(id))
        return cur.lastrowid

def purge_messages(id):
    con = mdb.connect(db_host, db_user, db_password, db_name, connect_timeout=5)
    with con:
        cur = con.cursor()
        cur.execute("UPDATE USER_FEED_PROCESSOR SET News=NULL WHERE id = " + str(id))
        return cur.lastrowid

def find_news_for_ids(ids):
    con = mdb.connect(db_host, db_user, db_password, db_name, connect_timeout=5)
    with con:
        cur = con.cursor()
        cur.execute("SELECT * FROM NEWS_FEED_PROCESSOR WHERE ID  IN (" + ids +")")
        rows = cur.fetchall()
        return rows

def find_news_for_user_category(category):
    con = mdb.connect(db_host, db_user, db_password, db_name, connect_timeout=5)
    with con:
        cur = con.cursor()
        cur.execute("SELECT * FROM NEWS_FEED_PROCESSOR WHERE Category = '" + category + "'")
        rows = cur.fetchall()
        return rows


def get_newsids_for_user(id):
    con = mdb.connect(db_host, db_user, db_password, db_name, connect_timeout=5)
    with con:
        cur = con.cursor()
        cur.execute(
            "SELECT  DISTINCT(News) FROM USER_FEED_PROCESSOR  WHERE id =" + str(id) + " AND News IS NOT NULL")
        rows = cur.fetchall()
        return rows


def get_user_subscription(id):
    con = mdb.connect(db_host, db_user, db_password, db_name, connect_timeout=5)
    with con:
        cur = con.cursor()
        cur.execute("SELECT  DISTINCT(Subscription) FROM USER_FEED_PROCESSOR  WHERE id =" + str(
            id) + " AND Subscription IS NOT NULL")
        rows = cur.fetchall()
        return rows


def get_news(newsdata):
    print("News Record:")
    print(newsdata)
    news = {
        'title': str(newsdata[1]).replace("'", "\\'"),
        'description': str(newsdata[2]).replace("'", "\\'"),
        'link': str(newsdata[4]).replace("'", "\\'")
    }
    return news


def process_user_subscription(id):
    subscriptions = get_user_subscription(id)
    newsdata = []
    if len(subscriptions) > 0:
        for subscription in subscriptions:
            print("Finding news for user.")
            allnews = find_news_for_user_category(subscription[0])
            print(allnews)
            if len(allnews) > 0:
                for news in allnews:
                    newsdata.append(str(news[0]))
                data = ','.join(newsdata)
                print(data)
                print("Updating news details for user.")
                update_news_for_user(id, data)

@app.route('/')
def run():
    return 'Filtering App is Up & Running. Give appropriate REST endpoints.\n', 200


@app.route('/ready', methods=['GET'])
def ready():
    create_table()
    return 'Table is Created and Ready to Use.\n', 200


@app.route('/health', methods=['GET'])
def health():
    con = mdb.connect(db_host, db_user, db_password, db_name, connect_timeout=5)
    with con:
        return 'Able to Ping DB. Healthy Connection.\n', 200
    return 'Not able to Ping DB. Un-Healthy Connection.\n', 400

def get_json_for_news(rows):
    newsfeeds = []
    if len(rows) > 0:
        for news in rows:
            newsdata = get_news(news)
            print(json.dumps(newsdata))
            newsfeeds.append(newsdata)
        return jsonify({'news': newsfeeds})

@app.route('/users/<int:id>/getmessages', methods=['GET'])
def process_messages_for_user(id):
    print("Reading user Subscription")
    process_user_subscription(id)
    print("Getting user messages")
    con = mdb.connect(db_host, db_user, db_password, db_name, connect_timeout=5)
    print(con)
    with con:
        rows = get_newsids_for_user(id)
        ids = str(rows[0])
        ids = ids[1:-2]
        ids = ids.replace(",","','")
        rows = find_news_for_ids(ids)
        rows = json.dumps(rows)
        print(rows)
    if len(rows) < 1:
        abort(404)
    resp = Response(response=rows,
                    status=200,
                    mimetype="application/json")
    return resp


@app.route('/get/news/<int:id>', methods=['GET'])
def process_messages_for_user_json(id):
    filename = "/app/output.json"
    data = read_file(filename)
    resp = Response(response=data,
                    status=200,
                    mimetype="application/json")
    return resp


def purge_messages_for_user(id):
    print("Purge user Messages.")
    purge_messages(id)
    return make_response(jsonify({'OK': 'News Deleted For User'}), 200)


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'OK': 'Wrong Endpoints or Message Not found.'}), 404)


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
